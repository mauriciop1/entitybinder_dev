package org.camel.integrationbinder;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

class StringAggregationStrategy implements AggregationStrategy {

    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {

        if (oldExchange == null) {
           return newExchange;
        }

        String oldBody = oldExchange.getIn().getBody(String.class);
        String newBody = newExchange.getIn().getBody(String.class);

        if (!oldBody.contains("<Costumer>"))
            oldExchange.getIn().setBody("<Costumer>" + oldBody + newBody);
          else
            oldExchange.getIn().setBody(oldBody + newBody + "</Costumer>");
                                        
        return oldExchange;
    }
}
