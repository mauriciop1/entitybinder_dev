package org.camel.integrationbinder;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "org.camel.integrationbinder.OrderService")
public class OrderServiceImpl implements OrderService {

    @Override
    public String[] getOrders() {
        return new String[]{"SSD", "Graphic Card", "GPU"};
    }

    @Override
    public String addOrder(@WebParam(name = "bazArg") String order) {
        System.out.println("Saving new order: " + order);
        return "Saving new order: " + order;
    }
}