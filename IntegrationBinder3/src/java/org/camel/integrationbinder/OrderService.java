package org.camel.integrationbinder;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;


@WebService
@SOAPBinding(style = Style.RPC)
public interface OrderService {

    @WebMethod
    String[] getOrders();

    @WebMethod
    String addOrder(@WebParam(name = "bazArg") String order);

}