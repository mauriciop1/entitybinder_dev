package org.camel.integrationbinder;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class HelloWorldResource implements HelloWorldIntf {
    public Response greet() {

        return Response.status(Status.OK).entity("This is a simple GET! We can put here anything!").build();
    }

    public Response sayHello(String input) {
        Hello hello = new Hello();
        hello.setHello("I Can put anything here!");
        hello.setName("Default User");

        if (input != null)
            hello.setName(input);

        return Response.status(Status.OK).entity(hello).build();
    }
}

class Hello {
    private String accntno;
    private String name;

    public String getHello() {
        return accntno;
    }

    public void setHello(String accnt) {
        this.accntno = accnt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
