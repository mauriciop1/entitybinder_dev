package org.camel.integrationbinder;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.SpringCamelContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import org.camel.integrationbinder.ServerInfo;
import org.camel.integrationbinder.OrderServiceImpl;

import javax.xml.ws.Endpoint;

public class CamelCxfExample extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
        try {
            System.out.println("@Running CamelCXF Code...");
            main();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    public static void main() throws Exception {
        System.setProperty("port1", "9290");
        
        Endpoint.publish("http://localhost:8888/ws/server", new ServerInfo());
        Endpoint.publish("http://localhost:8888/service/order", new OrderServiceImpl());
        

        // Creation of routes through application Context
        ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        ApplicationContext appContext2 = new ClassPathXmlApplicationContext("applicationContext2.xml");
        ApplicationContext appContext3 = new ClassPathXmlApplicationContext("camel-cxfrs-config.xml");

        CamelContext camelContext = new SpringCamelContext(appContext);
        CamelContext camelContext2 = new SpringCamelContext(appContext2);
        CamelContext camelContext3 = new SpringCamelContext(appContext3);
        try {

            System.out.println("STARTING camel Context!");
            camelContext.start();
            camelContext2.start();
            camelContext3.start();
            
            System.out.println("STARTED camel Context!");

            // Thread.sleep(Long.MAX_VALUE);
        } finally {
            System.out.println("STOP Camel Context!");
            // camelContext.stop();
        }
    }

}
