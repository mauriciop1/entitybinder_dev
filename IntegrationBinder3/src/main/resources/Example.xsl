<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output omit-xml-declaration="no" indent="yes" />
	<xsl:strip-space elements="*" />
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" />
		</xsl:copy>
	</xsl:template>
	<xsl:template match="name">
		<NAME>
			<xsl:value-of select="." />
		</NAME>
	</xsl:template>
	<xsl:template match="accntno">
		<ACCOUNT_NO>
			<xsl:value-of select="." />
		</ACCOUNT_NO>
	</xsl:template>
	<xsl:template match="address">
		<ADDRESS>
			<xsl:value-of select="." />
		</ADDRESS>
	</xsl:template>
</xsl:stylesheet>